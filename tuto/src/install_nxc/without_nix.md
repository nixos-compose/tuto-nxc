# Installing NixOS Compose

We can install NixOS Compose via `pip`:

```sh
pip install nixos-compose
```


You might need to modify your `$PATH`:

```sh
echo "export PATH=$PATH:~/.local/bin" >> ~/.bash_profile
source ~/.bash_profile
```

