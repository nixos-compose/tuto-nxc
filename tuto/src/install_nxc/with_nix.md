## With Nix

To simplify the use of NixOS Compose, we will add it to our registries:

```bash
nix registry add nxc git+https://gitlab.inria.fr/nixos-compose/nixos-compose 
```

**TODO:** maybe add a tag to be sure?

You should now be able to run

```bash
nix flake show nxc
```

which would return something like:

```
git+https://gitlab.inria.fr/nixos-compose/nixos-compose?ref=master&rev=0a8c43a99cfa6bbace6d391a51cd761c6fec2bd9
├───defaultPackage
│   ├───aarch64-darwin: package 'python3.9-nxc-locale'
│   ├───aarch64-linux: package 'python3.9-nxc-locale'
│   ├───i686-linux: package 'python3.9-nxc-locale'
│   ├───x86_64-darwin: package 'python3.9-nxc-locale'
│   └───x86_64-linux: package 'python3.9-nxc-locale'
├───devShell
│   ├───aarch64-darwin: development environment 'nix-shell'
│   ├───aarch64-linux: development environment 'nix-shell'
│   ├───i686-linux: development environment 'nix-shell'
│   ├───x86_64-darwin: development environment 'nix-shell'
│   └───x86_64-linux: development environment 'nix-shell'
├───devShells
│   ├───aarch64-darwin
│   │   ├───nxcShell: development environment 'nix-shell'
│   │   └───nxcShellFull: development environment 'nix-shell'
│   ├───aarch64-linux
│   │   ├───nxcShell: development environment 'nix-shell'
│   │   └───nxcShellFull: development environment 'nix-shell'
│   ├───i686-linux
│   │   ├───nxcShell: development environment 'nix-shell'
│   │   └───nxcShellFull: development environment 'nix-shell'
│   ├───x86_64-darwin
│   │   ├───nxcShell: development environment 'nix-shell'
│   │   └───nxcShellFull: development environment 'nix-shell'
│   └───x86_64-linux
│       ├───nxcShell: development environment 'nix-shell'
│       └───nxcShellFull: development environment 'nix-shell'
├───lib: unknown
├───overlay: Nixpkgs overlay
├───packages
│   ├───aarch64-darwin
│   │   ├───doc: package 'nxcDoc'
│   │   ├───imageCIdoc: package 'docker-image-nxc-doc.tar.gz'
│   │   ├───nixos-compose: package 'python3.9-nxc-locale'
│   │   └───showTemplates: package 'templates.json'
│   ├───aarch64-linux
│   │   ├───doc: package 'nxcDoc'
│   │   ├───imageCIdoc: package 'docker-image-nxc-doc.tar.gz'
│   │   ├───nixos-compose: package 'python3.9-nxc-locale'
│   │   └───showTemplates: package 'templates.json'
│   ├───i686-linux
│   │   ├───doc: package 'nxcDoc'
│   │   ├───imageCIdoc: package 'docker-image-nxc-doc.tar.gz'
│   │   ├───nixos-compose: package 'python3.9-nxc-locale'
│   │   └───showTemplates: package 'templates.json'
│   ├───x86_64-darwin
│   │   ├───doc: package 'nxcDoc'
│   │   ├───imageCIdoc: package 'docker-image-nxc-doc.tar.gz'
│   │   ├───nixos-compose: package 'python3.9-nxc-locale'
│   │   └───showTemplates: package 'templates.json'
│   └───x86_64-linux
│       ├───doc: package 'nxcDoc'
│       ├───imageCIdoc: package 'docker-image-nxc-doc.tar.gz'
│       ├───nixos-compose: package 'python3.9-nxc-locale'
│       └───showTemplates: package 'templates.json'
└───templates
    ├───basic: template: Minimal example
    ├───basic-flake-only: template: Basic example with nix dir via flake
    ├───basic-nur: template: Basic example using a NUR
    ├───execo: template: Template with execo
    ├───kernel: template: Example with different Linux Kernels
    ├───multi-compositions: template: A multi-compositions example
    ├───nixos-cluster: template: A remote store for nixos machines
    ├───scripts: template: Scripts example
    ├───setup: template: Setup example
    ├───spark: template: Spark example
    └───webserver: template: A nginx example
```
