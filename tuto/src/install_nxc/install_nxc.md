# Installing NixOS Compose

## The python API

We can install NixOS Compose via `pip`:

```sh
pip install nixos-compose
```


You might need to modify your `$PATH`:

```sh
echo "export PATH=$PATH:~/.local/bin" >> ~/.bash_profile
source ~/.bash_profile
```

## The nix package manager

NixOS-Compose unsurprisingly needs Nix. Let us install it locally:

```sh
nxc helper install-nix
```

This will put the nix store in ~/.local/share

## Set up a preloaded store

To accelerate a bit the tutorial, we will preload the store to avoid having to download a lot of packages:


```sh
curl -sL https://gitlab.inria.fr/nixos-compose/tuto-nxc/-/raw/main/misc/import-base-store.sh | bash
```
