## Run the benchmark in the environment


As done previously, we have to go through the `nxc build` and `nxc start` phases.

Once the environment deployed, we connect with `nxc connect`


We can now try to run the benchmark.

```bash
mpirun -np 4 MADbench2.x 640 8 1 8 8 1 1
```

Unfortunalety this will fail for several reasons:

- we are running as root

- there are not enough slots to start 4 processes

As we are setting up the environment for now, we can disable these warnings by adding some flags:

```bash
mpirun --allow-run-as-root --oversubscribe -np 4 MADbench2.x 640 8 1 8 8 1 1
```
