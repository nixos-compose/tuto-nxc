## Adding MADbench2 to the composition

Once `MADbench2` has been added to the `flake.nix` file, we can access the package inside the composition.
In our case, we want an environment with both `MADbench2` and `openmpi` to run it.


```nix
{ pkgs, ... }: {
  roles = {
    foo = { pkgs, ... }:
      {
        # add needed package
        environment.systemPackages = with pkgs; [ openmpi MADbench2 ];
      };
  };
  testScript = ''
    foo.succeed("true")
  '';
}
```
