## Making MADbench2 available

### Option 1: You have a flake reprository with MADbench2 packaged


In the `flake.nix` file, add your reprository as an input:

```nix
{
  description = "nixos-compose - basic setup";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/21.05";
    nxc.url = "git+https://gitlab.inria.fr/nixos-compose/nixos-compose.git";
    mypkgs.url = "URL TO YOUR PKGS";
  };
  outputs = { self, nixpkgs, nxc, mypkgs }:
  {
    # ...
  };
}
```

Then create an overlay with adding the benchmark:

```nix
{
  # ...

  outputs = { self, nixpkgs, nxc, mypkgs }:
    let
      system = "x86_64-linux";
      myOverlay = final: prev:
        {
          mb2 = mypkgs.packages.${system}.MADbench2;
        };
    in {
      packages.${system} = nxc.lib.compose {
        inherit nixpkgs system;
        overlays = [ myOverlay ];
        composition = ./composition.nix;
        # setup = ./setup.toml;
      };

      # ...
};
```


### Option 2: You have a local file packaging MADbench2

In this case, you do not have to add another input, and you can simply call the Nix file packaging MADbench2 (`MADbench2.nix` in the example below).

Similarly, we create an overlay with the package to add.

```nix
{
  description = "nixos-compose - basic setup";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/22.05";
    nxc.url = "git+https://gitlab.inria.fr/nixos-compose/nixos-compose.git";
  };

  outputs = { self, nixpkgs, nxc }:
    let
      system = "x86_64-linux";
      myOverlay = final: prev:
        {
          mb2 = prev.callPackage ./MADbench2 { };
        };
    in {
      packages.${system} = nxc.lib.compose {
        inherit nixpkgs system;
        overlays = [ myOverlay ];
        composition = ./composition.nix;
        setup = ./setup.toml;
      };

      defaultPackage.${system} =
        self.packages.${system}."composition::nixos-test";

      devShell.${system} = nxc.devShells.${system}.nxcShellFull;
    };
}
```
