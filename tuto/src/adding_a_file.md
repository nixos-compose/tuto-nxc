# Adding a configuration file

IOR allows to take a configuration file as input.
In this section we will integrate this configuration file into the configuration.


A IOR configuration file looks something like this:

```
IOR START
    api=POSIX
    testFile=testFile
    hintsFileName=hintsFile
    multiFile=0
    interTestDelay=5
    readFile=1
    writeFile=1
    filePerProc=0
    checkWrite=0
    checkRead=0
    keepFile=1
    quitOnError=0
    outlierThreshold=0
    setAlignment=1
    singleXferAttempt=0
    individualDataSets=0
    verbose=0
    collective=0
    preallocate=0
    useFileView=0
    keepFileWithError=0
    setTimeStampSignature=0
    useSharedFilePointer=0
    useStridedDatatype=0
    uniqueDir=0
    fsync=0
    storeFileOffset=0
    maxTimeDuration=60
    deadlineForStonewalling=0
    useExistingTestFile=0
    useO_DIRECT=0
    showHints=0

    repetitions=3
    numTasks=16
    segmentCount=16
    blockSize=4k
    transferSize=1k

    summaryFile=/tmp/results_ior.json
    summaryFormat=JSON
RUN
IOR STOP
```

It gathers the information about the experiment.

It can then be run as:

```
ior -f <IOR_CONFIG_FILE>
```

We will put this configuration file in the composition in order to make the experiment reproducible.
Let us store this file locally in the file `script.ior`.

We can then create a file in `/etc/` that will point to the content of the file in the nix store.
In the following configuration we will write the file at `/etc/ior_script`:


```nix
# ...
  node = { pkgs, ... }: {
    networking.firewall.enable = false;

    environment.systemPackages = with pkgs; [ openmpi ior ];

    environment.etc = {
      ior_script = {
        text = builtins.readFile ./script.ior;
      };
    };

    fileSystems."/data" = {
      device = "server:/";
      fsType = "nfs";
    };
  };
# ...
```


## Building

```
nxc build -f g5k-nfs-store
```


## Deploying

### Reserving the resources

```sh
export $(oarsub -l nodes=2,walltime=1:0:0 "$(nxc helper g5k_script) 1h" | grep OAR_JOB_ID)
```

### Starting the nodes

```sh
nxc start -m OAR.$OAR_JOB_ID.stdout -W
```

## Connecting

```sh
nxc connect
```

## Running the benchmark with the script

From the `node`:

```sh
cd /data
ior -f /etc/ior_script
```

### Release the nodes

```sh
oardel $OAR_JOB_ID
```