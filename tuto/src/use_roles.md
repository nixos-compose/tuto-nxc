# Generalization of the composition

As you can see from the previous section, the scaling of the number of computing node is a bit cumbersome.

Fortunately, NixOS Compose provides the notion of *role* to tackle this issue.

A *role* is a configuration.
In our case, we actually have only two roles: the NFS server and the compute nodes.
The configuration of the compute nodes is the same no matter how many compute nodes.
Thus having to define the configuration for `node1` and `node2` is redundant.

```nix
roles = {

  node = { pkgs, ... }:
  {
    # add needed package
    environment.systemPackages = with pkgs; [ openmpi ior ];
  
    # Disable the firewall
    networking.firewall.enable = false;
  
    # Mount the NFS
    fileSystems."/data" = {
      device = "server:/";
      fsType = "nfs";
    };
  };

  server = { pkgs, ... }:
  { # ... };
}
```

## Building

```sh
nxc build -f g5k-nfs-store
```


## Deploying

### Reserving the resources

```sh
export $(oarsub -l nodes=3,walltime=1:0:0 "$(nxc helper g5k_script) 1h" | grep OAR_JOB_ID)
```

### Starting the nodes

The `nxc start` command can take an additional yaml file as input describing the number of machines per role, as well as their hostnames.

```sh
nxc start -m OAR.$OAR_JOB_ID.stdout -W nodes.yaml
```

The following `yaml` file will create 3 machines: `server`, `node1` and `node2`.

```yaml
# nodes.yaml
node: 2
```


### Connect to the nodes

```sh
nxc connect
```

### Release the nodes

```
oardel $OAR_JOB_ID
```
