# Create the `setup.toml` file

Create a `setup.toml` file and add the following content:

```toml
# setup.toml

[project]

[params]
nfsNbProcs=8
```

Don't forget to add `setup.toml` to the git:

```
git add setup.toml
```

