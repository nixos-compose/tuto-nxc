# Introduce variations

For the moment, we only have a single value for the parameter.
But we want to have variation.
In the current situation, we have a NFS server with 8 processes managing the requests.
We want to see how this number of processes influences the performances of the IOR benchmark.
We will create two variants:
- `fast`: with more processes for the NFS server (let us say 32)
- `slow`: with fewer processes (let us say 2)


We can add the variants in the `setup.toml` file as follows:


```toml
# setup.toml

[project]

[params]
nfsNbProcs=8

[slow.params]
# to select this variant use:
# nxc build --setup slow
nfsNbProcs=2

[fast.params]
# to select this variant use:
# nxc build --setup fast
nfsNbProcs=32
```


When we build the composition we can pick the variant as:

```
nxc build --setup fast
# or
nxc build -s slow
```

If there is no `--setup` or `-s` flag, NixOS Compose will take the default values of the parameters.
