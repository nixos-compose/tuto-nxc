# Parametrized Builds

We often want to have variation in the environment.
In our example, it could be the configuration of the NFS server.

One solution could be to have several `composition.nix` files for each variant, but there is a lot of redundancy.

Instead, we will use the notion of `setup` of NixOS Compose to parametrize the composition.
