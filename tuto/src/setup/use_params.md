# Use the paramaters

The above `setup.toml` defines a paramater called `nfsNbProcs`.
The value of this paramater can be used in the composition.
The value of the paramater is available at `setup.param.<VARIABLE_NAME>`.

```nix
# composition.nix

# Add `setup` in the arguments of the composition
{ pkgs, setup, ... }: {
  # ....
  server = { pkgs, ... }: {
    networking.firewall.enable = false;
    services.nfs.server.enable = true;
    services.nfs.server.exports = ''
      /srv/shared *(rw,no_subtree_check,fsid=0,no_root_squash)
    '';
    services.nfs.server.createMountPoints = true;

    # Use the value of the paramater
    services.nfs.server.nproc = setup.params.nfsNbProcs;

    environment.systemPackages = with pkgs; [ htop ];
  };
}
```
