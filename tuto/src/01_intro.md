# Introduction

This tutorial presents NixOS Compose.
This tool generates and deploy fully reproducible system environments.
It relies on the Nix package manager and the associated Linux distribution NixOS.

You can find the associated publication [here](https://hal.archives-ouvertes.fr/hal-03723771).
And some partial documentation [here](https://nixos-compose.gitlabpages.inria.fr/nixos-compose/).


First, we will set up the connection to the Grid'5000 machines.
Then we will present NixOS Compose on a simple example to get you acquainted with the notions and commands.
Finally, we will create step by step a reproducible environment for a distributed experiment.


Note: Beside some SSH configuration, all of the commands will be executed on the Grid'5000 platform.
