# Create your first composition

Before jumping into the creation of an environment for the IOR benchmark, let us go through a simpler example.

In the next sections, we will present the notions and commands of NixOS Compose.
