# Summary

- [Introduction](01_intro.md)

# Setup

- [Grid'5000](g5k.md)
- [Installing NixOS Compose](install_nxc/install_nxc.md)

# An Overview of NixOS Compose

- [First Composition](02_first_composition.md)
    - [Starting from a template](first_composition/start_template.md)
    - [Inspect the composition](first_composition/inspect_composition.md)
    - [Local Build using Virtual Machines](first_composition/vm.md)
    - [Build the composition](first_composition/build_first_composition.md)
    - [Deploy the composition](first_composition/deploy_composition.md)
        - [`g5k-nfs-store`](first_composition/g5k-nfs-store.md)
        - [`g5k-image`](first_composition/g5k-image.md)
    - [Connect to the nodes](first_composition/connect.md)
    - [Release the nodes](first_composition/relase_node.md)

# Creating an Reproducible Experiment

- [IOR in the Composition](ior_in_composition/README.md)
    - [Adding IOR to the environment](ior_in_composition/adding_ior.md)
    - [Run the benchmark](ior_in_composition/run_benchmark.md)
- [Adding a NFS server](add_nfs/03_add_nfs.md)
    - [Adding a new role](add_nfs/add_new_role.md)
    - [Setting up the NFS server](add_nfs/setting_nfs_server.md)
    - [Mount the NFS server](add_nfs/mount_nfs_server.md)
    - [Testing the NFS server](add_nfs/testing.md)
- [Adding another compute node](04_add_another_node.md)
- [Generalization of the composition](use_roles.md)
- [Adding a configuration file](adding_a_file.md)
- [Adding scripts](05_add_scripts.md)
- [Parametrized Builds](setup/setup.md)
    - [Update the flake](setup/update_flake.md)
    - [Create the `setup.toml`](setup/create_setup.md)
    - [Use parameters](setup/use_params.md)
    - [Introduce variations](setup/introduce_variations.md)
- [Integration with an Experiment Engine](execo.md)


# To go further

- [Package MADBench2](01_mb2.md)
- [MADBench2 in the Composition](02_first_composition.md)
    - [Importing the Nix Expressions](mb2_in_composition/mb2_available.md)
    - [Adding MADBench2 to the environment](mb2_in_composition/adding_mb2.md)
    - [Run the benchmark](mb2_in_composition/run_benchmark.md)
- [Add an external builder on Grid'5000](appendices/g5k_builder.md)