# Add a builder on Grid'5000

If you are doing this tutorial with a member of the NixOS Compose team, there is probably a builder machine on Grid'5000 to accelerate the builds.
Using it requires a bit a configuration.

Ask for the `<BUILDER>` address.
It should look like `dahu-30`.

## Add your ssh key

You need to get access to the builder machine through `ssh`.
To do this we will copy your ssh key to the builder.

```sh
ssh-copy-id -i ~/.ssh/id_rsa.pub root@<BUILDER>
```

The password is `nixos`.


You should be able to log to the `<BUILDER>` via `ssh root@<BUILDER>`.


## Copy the Nix Config

We need to tell Nix where to find this builder and how to use it.
The configuration file should be under `~/.config/nix/nix.conf`.


```
# ~/.config/nix/nix.conf
experimental-features = nix-command flakes
builders = ssh://root@<BUILDER>
cores = 0
extra-sandbox-paths =
max-jobs = 0
require-sigs = false
sandbox = false
sandbox-fallback = false
substituters = http://<BUILDER>:8080 ssh-ng://root@<BUILDER> https://cache.nixos.org/
builders-use-substitutes = true
trusted-public-keys = <BUILDER>:snBDi/dGJICacgRUw4nauQ8KkSksAAAhCvPVr9OGTwk=
system-features = nixos-test benchmark big-parallel kvm
allowed-users = *
trusted-users = root
```

Copy paste the following configuration in the nix configuration file.
**Don't forget to replace `<BUILDER>` with the actual address of the builder!**.

You can do it with one command. We take here the example where the builder in on node `dahu-30`.

```
sed -i 's/<BUILDER>/dahu-30/g' ~/.config/nix/nix.conf
```

Note that this configuration will not work the next time you log to Grid'5000 as the builder node would have been released.
You can revert back to:

```
# ~/.config/nix/nix.conf
experimental-features = nix-command flakes
max-jobs = 64
```
