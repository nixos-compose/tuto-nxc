# Grid'5000

Grid'5000 (a.k.a. g5k) is a french testbed for distributed experiments.

This tutorial relies on this platform to perform deployments.
We will thus create accounts for you.

Hopefully, by the time you read these lines, your account should have already been created.


## Email

You should receive an email like:

```
Subject: [Grid5000-account] Your Grid5000 account was created by ...

Dear Firstname Lastname (username),

You receive this email because your manager (...) requested a Grid5000
account for you in the context of a tutorial. To get more information about
Grid5000, see the website: http://www.grid5000.fr.

Your login on the Grid5000 platform is: username.

The next two steps for you are now to:

1/ Finish setting up your access to the platform by creating a password and an SSH key.

   To do so, open the following URL:
   https://public-api.grid5000.fr/stable/users/setup_password?password_token=XXXXXXXXXXXXXXXXXXXXXX#special.

2/ Read carefully the two following pages:

   The Grid5000 getting started documentation (https://www.grid5000.fr/w/Getting_Started),
   which gives important information on how to use the platform.

   The Grid5000 usage policy (https://www.grid5000.fr/w/Grid5000:UsagePolicy),
   which gives the rules that MUST be followed when using the platform. Note that any
   abuse will automatically be detected and reported to your manager.
```

Follow the steps in the email before continuing.


## Connect to Grid'5000

Add these lines to your `~/.ssh/config` file and replace `G5K_USERNAME` by your username:

```
Host g5k
  User G5K_USERNAME
  Hostname access.grid5000.fr
  ForwardAgent no
Host *.g5k
  User G5K_USERNAME
  ProxyCommand ssh g5k -W "$(basename %h .g5k):%p"
  ForwardAgent no
```

You should be able to access the different sites of Grid'5000:


```
ssh grenoble.g5k
```

We will use the Grenoble site for this tutorial.


## Tips

SSH connections can be broken, which is quite annoying when doing experiments.
We thus recommend to use `tmux` to deal with it.


## First job reservation

You can try to make a job reservation.
Grid'5000 uses OAR as a resources job manager.

```
oarsub -I
```

- `oarsub` is the command to make a submission

- `-I` means that this is an interactive job



This command should give you access to a node that is not the frontend (`fgrenoble`), but a node from the   `dahu` cluster (`dahu-X`).
You can execute some commands there.
And once you are done, just exit the shell, and you will be back on the frontend.
As this is an interactive job, the job will be killed as soon as you exit the shell.

## `tmux`

We recommend using [`tmux`](https://github.com/tmux/tmux) on Grid'5000 as the connection between your laptop and Grid'5000 could break, and you could lose access to your work.

A cheat-sheet of `tmux` is available here: [https://tmuxcheatsheet.com/](https://tmuxcheatsheet.com/).