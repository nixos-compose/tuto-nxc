## Mount the NFS server on the compute nodes


We now need to make the compute nodes mount the NFS server.

To do this, we will also disable the firewall and create a new mounting point (`/data` in our case):

```nix
node = { pkgs, ... }:
{
  # add needed package
  environment.systemPackages = with pkgs; [ openmpi ior ];

  # Disable the firewall
  networking.firewall.enable = false;

  # Mount the NFS
  fileSystems."/data" = {
    device = "server:/";
    fsType = "nfs";
  };
};
```
