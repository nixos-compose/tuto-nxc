## Setting up the NFS server


To set up the NFS server, we need to configure several things:

1. open ports for the clients to connect: we will actually disable the entire firewall for simplicity stakes, but we could be more precise in the ports we open

2. Enable the `systemd` service for the NFS server

3. Define the export point `/srv`


```nix
server = { pkgs, ... }: {
  # Disable the firewall
  networking.firewall.enable = false;

  # Enable the nfs server services
  services.nfs.server.enable = true;

  # Define a mount point at /srv
  services.nfs.server.exports = ''
    /srv *(rw,no_subtree_check,fsid=0,no_root_squash)
  '';
  services.nfs.server.createMountPoints = true;

  # we also add the htop package for light monitoring
  environment.systemPackages = with pkgs; [ htop ];
};
```
