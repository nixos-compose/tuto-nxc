# Test the NFS server


## Building

```
nxc build -f g5k-nfs-store
```


## Deploying

### Reserving the resources

This time **two** nodes.

```
export $(oarsub -l nodes=2,walltime=1:0:0 "$(nxc helper g5k_script) 1h" | grep OAR_JOB_ID)
```

### Starting the nodes

```sh
nxc start -m OAR.$OAR_JOB_ID.stdout -W
```

## Connect

```
nxc connect
```

You should now have a `tmux` session with 2 panes: one for the `server` and one for the `node`.

## Testing the NFS server


Go on the `node` and change directory to the mounted NFS:

```
cd /data
```

A quick `ls` shows that this is empty.

Create a file in it:

```
touch nfs_works
```


Now go on the `server` node and check the directory of the NFS server.

```
ls /srv
```

If the `nfs_works` file exists, everything worked fine!


### Release the nodes

```
oardel $OAR_JOB_ID