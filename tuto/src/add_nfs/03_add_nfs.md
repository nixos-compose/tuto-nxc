# Add an NFS server to the composition

For the moment the tests are only performed on the local file system of one computer.

Let us to setup a distributed filesystem to evaluate its performances.
For this tutorial, it will be NFS.

The approach will be the following:

1. add a new role `server` to the composition

2. setup the NFS server on the `server` role

3. mount the NFS export on the compute nodes
