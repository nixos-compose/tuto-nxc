## Add a role to the composition


To add another role to the composition, we only need to add a new element to the set `roles`.
Let us also rename the compute node to `node` and empty the `testScript`:

```nix
{ pkgs, ... }: {
  roles = {
    node = { pkgs, ... }:
      {
        # add needed package
        environment.systemPackages = with pkgs; [ openmpi ior ];
      };
    server = { pkgs, ... }:
      {
        # ...
      };
  };
  testScript = ''
  '';
}
```
