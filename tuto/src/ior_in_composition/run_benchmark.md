# Run the benchmark in the environment


As done previously, we have to go through the `nxc build` and `nxc start` phases.


## Building

```sh
nxc build -f g5k-nfs-store
```

## Getting the node

```sh
export $(oarsub -l nodes=1,walltime=1:0:0 "$(nxc helper g5k_script) 1h" | grep OAR_JOB_ID)
```


## Deploying

```sh
nxc start -m OAR.$OAR_JOB_ID.stdout -W
```


## Running the benchmark


Once the environment is deployed, we connect with `nxc connect`.


We can now run the benchmark.

```sh
ior
```

The output should look something like that:

```sh
[root@foo:~]# ior
IOR-3.3.0: MPI Coordinated Test of Parallel I/O
Began               : Tue Sep 13 14:08:28 2022
Command line        : ior
Machine             : Linux foo
TestID              : 0
StartTime           : Tue Sep 13 14:08:28 2022
Path                : /root
FS                  : 1.9 GiB   Used FS: 1.3%   Inodes: 0.5 Mi   Used Inodes: 0.1%

Options:
api                 : POSIX
apiVersion          :
test filename       : testFile
access              : single-shared-file
type                : independent
segments            : 1
ordering in a file  : sequential
ordering inter file : no tasks offsets
nodes               : 1
tasks               : 1
clients per node    : 1
repetitions         : 1
xfersize            : 262144 bytes
blocksize           : 1 MiB
aggregate filesize  : 1 MiB

Results:

access    bw(MiB/s)  IOPS       Latency(s)  block(KiB) xfer(KiB)  open(s)    wr/rd(s)   close(s)   total(s)   iter
------    ---------  ----       ----------  ---------- ---------  --------   --------   --------   --------   ----
write     3117.60    13205      0.000076    1024.00    256.00     0.000014   0.000303   0.000004   0.000321   0
read      3459.03    15107      0.000066    1024.00    256.00     0.000022   0.000265   0.000002   0.000289   0
remove    -          -          -           -          -          -          -          -          0.000121   0
Max Write: 3117.60 MiB/sec (3269.04 MB/sec)
Max Read:  3459.03 MiB/sec (3627.06 MB/sec)

Summary of all tests:
Operation   Max(MiB)   Min(MiB)  Mean(MiB)     StdDev   Max(OPs)   Min(OPs)  Mean(OPs)     StdDev    Mean(s) Stonewall(s) Stonewall(MiB) Test# #Tasks tPN reps fPP reord reordoff reordrand seed segcnt   blksiz    xsize aggs(MiB)   API RefNum
write        3117.60    3117.60    3117.60       0.00   12470.38   12470.38   12470.38       0.00    0.00032         NA            NA     0      1   1    1   0     0        1         0    0      1  1048576   262144       1.0 POSIX      0
read         3459.03    3459.03    3459.03       0.00   13836.14   13836.14   13836.14       0.00    0.00029         NA            NA     0      1   1    1   0     0        1         0    0      1  1048576   262144       1.0 POSIX      0
Finished            : Tue Sep 13 14:08:28 2022
```

## Release the booking

Now we are done, we exit the connection to the node and release this reservation.

```shell
oardel $OAR_JOB_ID
```