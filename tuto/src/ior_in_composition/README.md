# IOR

IOR is a parallel IO benchmark that can be used to test the performance of parallel storage systems using various interfaces and access patterns.
It uses a common parallel I/O abstraction backend and relies on MPI for synchronization.

You can find the documentation [here](https://ior.readthedocs.io/en/latest/intro.html)
