# Adding IOR to the composition

## Start from a template

```
mkdir ior_bench
cd ior_bench
nxc init -t basic
```

## Add IOR to the composition

The IOR benchmark is available in `nixpkgs` and thus accessible in `pkgs`.
We also need `openmpi` to run the benchmark.

```nix
# composition.nix
{ pkgs, ... }: {
  roles = {
    foo = { pkgs, ... }:
      {
        # add needed package
        environment.systemPackages = with pkgs; [ openmpi ior ];
      };
  };
  testScript = ''
    foo.succeed("true")
  '';
}
```
