# Deploying the `g5k-nfs-store` flavour

## Reserve the nodes

Let us reserve 1 machine for an hour on Grid'5000:

```sh
export $(oarsub -l nodes=1,walltime=1:0:0 "$(nxc helper g5k_script) 1h" | grep OAR_JOB_ID)
```

You can use `oarstat -u` to check the status of the reservation

## List the reserved machines

NixOS-Compose needs a list of target machines where to deploy the software environments. This list will be written to the file `OAR.$OAR_JOB_ID.stdout` in the current directory once the machines are available.

```shell
cat OAR.$OAR_JOB_ID.stdout
```

Which should output something like

```shell
dahu-2.grenoble.grid5000.fr
```

If the file does not exist yet, your reservation might not be ready yet. You can check with `oarstat -u`, looking for the "S" column. "W" means waiting and "R" means ready. In any case, the next command will wait for the creation of this file.

## Deploy

```sh
nxc start -m OAR.$OAR_JOB_ID.stdout -W
```