# Release the nodes

Once we are done with the experiment, we have to give back the resources.

## List all your running jobs

```
oarstat -u
```

```
Job id     Name           User           Submission Date     S Queue
---------- -------------- -------------- ------------------- - ----------
2155685                   qguilloteau    2022-09-23 10:50:36 R default
```

## Delete the job

```
oardel 2155685
```

```
Deleting the job = 2155685 ...REGISTERED.
The job(s) [ 2155685 ] will be deleted in the near future.
```

## The one-liner

You can delete all of your jobs with this one command:

```
oarstat -u -J | jq --raw-output 'keys | .[]' | xargs -I {} oardel {}
```
