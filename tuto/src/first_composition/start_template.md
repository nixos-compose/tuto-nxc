# Start from a template

NXC proposes several templates, which are good starting points.

Let us use the `basic` one.

```bash
mkdir tuto
cd tuto
nxc init -t basic
```


The previous command created 3 files in the `tuto` folder:

- `nxc.json`: a JSON file required by NXC. You never have to modify it.

- `flake.nix`: the Nix file responsible to lock all the inputs.

- `composition.nix`: the Nix file representing the distributed environment.


To ensure a good reproducibility of the composition, Nix (flake) uses Git to trace files.

Let us create a Git repository and add every created file to it:

```bash
git init
git add .
```

We should now commit:

```bash
git commit -m "NXC template files"
```
