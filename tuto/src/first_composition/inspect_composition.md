# Inspect the `composition.nix` file


If you open the `composition.nix` file, you will find the following:

```nix
{ pkgs, ... }: {
  roles = {
    foo = { pkgs, ... }:
      {
        # add needed package
        # environment.systemPackages = with pkgs; [ socat ];
      };
  };
  testScript = ''
    foo.succeed("true")
  '';
}
```

The composition is a function that takes a set as input (`{ pkgs, ... }`) and returns a set containing:

- a `testScript` string

- a `roles` set of NixOS configurations

What interest us for the moment is the `roles` set.
In the example above, we define a single role named `foo` with an empty configuration.
We can add packages to the environment by uncommenting the `environment.systemPackages` line:

```nix
{ pkgs, ... }: {
  roles = {
    foo = { pkgs, ... }:
      {
        # add needed package
        environment.systemPackages = with pkgs; [ socat ];
      };
  };
  testScript = ''
    foo.succeed("true")
  '';
}
```
