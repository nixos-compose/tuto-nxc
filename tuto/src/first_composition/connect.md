# Connect to the nodes

Once the deployment over, you can connect to the nodes via the `nxc connect` command.

```
nxc connect
```

It will open a tmux session with a pane per node, making it easy to navigate between the nodes.

You can provide a hostname to the command to connect to a specific host.


```
nxc connect foo
```
