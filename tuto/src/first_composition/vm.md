# Local deployment

In the NixOS Compose workflow, the idea is to first iterate with lightweight flavours such as `docker` or `vm`.

Before deploying at full scale to Grid'5000, let's try to deploy the environment locally on a single machine.

If you have a Linux machine you can try to run NixOS Compose on your machine, otherwise, you can also use Grid'5000.

Note: all the commands are to be executed from the root directory.

## On your local Linux machine

### Installing NixOS Compose

Please refer to the previous section to install NixOS Compose and Nix on your machine.

### Enter the Nix environment

```
nix develop
```

### Build the composition

```
nxc build -f vm
```

### Start the composition

```
nxc start
```

### Connect to the Virtual machines

In another terminal:

```
nxc connect
```

It should open a `tmux` window with access to the virtual machine.

## On Grid'5000

In this section, we present how to deploy Virtual Machines on a Grid'5000 nodes.
Note that this is not the common usage of NixOS Compose, and thus the workflow is a bit cumbersome.
For instance, make sure that you are not inside a `tmux` instance started on the frontend of the Grid'5000 site.

### Connect to a compute node

```
oarsub -I
```

### Start `tmux`

```
tmux
```

### Build the composition

```
nxc build -f vm
```

### Install `vde2`

In order to make the different virtual machines communicate, we need a virtual switch.
We use `vde2`, which we will install:

```
sudo-g5k apt install vde2
```

### Start the composition

```
nxc start
```

#### Connect

Create a new `tmux` pane (<kbd>⌃ Control</kbd> + <kbd>B</kbd> + <kbd>%</kbd>)

```
nxc connect
```

It should open a `tmux` window with access to the virtual machine.