# Deploying the `g5k-image` flavour

## Reserve the nodes

Let us deploy this composition on 1 physical machine

```sh
oarsub -l nodes=1,walltime=1:0:0 -t deploy
```

You can use `oarstat -u` to check the status of the reservation

## Deploy

```sh
nxc start -m OAR.$OAR_JOB_ID.stdout -W
```