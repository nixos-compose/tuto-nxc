## Deploy the environment

To deploy the built flavour use the `start` subcommand of `nxc`.
By default, it uses the last built environment from any flavour, but you can specify the flavour with the `-f` flag.

```bash
nxc start -f <FLAVOUR>
```


For the flavours on Grid'5000, we first need to reserve some resources before deploying.
The next sections give more details.
