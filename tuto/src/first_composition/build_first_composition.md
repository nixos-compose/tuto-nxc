# Build your first composition


You can build the composition with the `nxc build` command

It takes as argument a target platform, that we call *flavour*.

```
nxc build -f <FLAVOUR>
```

There are different flavours that NixOS Compose can build:

- `docker`: Generates a docker compose configuration

- `vm`: Generates Qemu Virtual Machines

- `g5k-ramdisk`: Generates Kernel images and initrd deployed with `kexec`

- `g5k-nfs-store`: Generates Kernel images and initrd without a packed `/nix/store`, but mounts the store of the frontend. Also deployed with `kexec`

- `g5k-image`: Generates full system image


**In this tutorial we will focus only on `g5k-nfs-store`, and `g5k-image` if you have time.**


For example, let us build the composition with the `g5k-nfs-store` flavour:

```
nxc build -f g5k-nfs-store
```
