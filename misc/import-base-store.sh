
echo "Import base store for NXC Tutorial from:"
echo "http://public.grenoble.grid5000.fr/~orichard/base_store.nar"

time NIX_IMPORTED_PATHS=$(curl -L http://public.grenoble.grid5000.fr/~orichard/nxc-tutorial-base-store.nar | exec -a nix-store nix --import)

echo "Number of imported paths:" $(echo $NIX_IMPORTED_PATHS | wc -w)
