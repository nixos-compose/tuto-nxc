{ pkgs, setup, ... }: {
  nodes = {
    node = { pkgs, ... }:
    let
      scripts = import ./myscripts.nix { inherit pkgs setup; };
    in
    {
      networking.firewall.enable = false;
      environment.systemPackages = with pkgs; [
        openmpi
        scripts.get_host_file
        ior
      ];
      fileSystems."${setup.params.nfsMountPoint}" = {
        device = "server:/";
        fsType = "nfs";
      };
    };
    server = { pkgs, ... }: {
      networking.firewall.enable = false;
      services.nfs.server.enable = true;
      services.nfs.server.exports = ''
        /srv/shared *(rw,no_subtree_check,fsid=0,no_root_squash)
      '';
      services.nfs.server.createMountPoints = true;
      services.nfs.server.nproc = setup.params.nfsNbProcs;
      environment.systemPackages = with pkgs; [ htop ];
    };
  };
}
