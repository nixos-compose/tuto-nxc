{ stdenv, openmpi }:

stdenv.mkDerivation {
  name = "MADbench2";
  src = ./.;
  buildInputs = [ openmpi ];
  installPhase = ''
    mkdir -p $out/bin
    mpicc -D SYSTEM -D COLUMBIA -D IO -o MADbench2.x MADbench2.c -lm
    mv MADbench2.x $out/bin
  '';
}
