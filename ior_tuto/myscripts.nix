{ pkgs, setup, ... }:
let
  nfsMountPoint = setup.params.nfsMountPoint; #"/data";
  nbProcs = setup.params.nbProcs;
  MB2Small = setup.params.MB2Small;
  MB2Med = setup.params.MB2Med;
  MB2Big = setup.params.MB2Big;
in rec {
  get_host_file = pkgs.writeScriptBin "get_host_file" ''
    NB_NODES=$(cat /etc/hosts | grep node | wc -l)
    NB_SLOTS_PER_NODE=$((${builtins.toString nbProcs} / $NB_NODES + 1))
    cat /etc/hosts | grep node | awk -v nb_slots="$NB_SLOTS_PER_NODE" '{ print $2 " slots=" nb_slots;}'

  '';

  start_mb_f = letter: pixels:
    pkgs.writeScriptBin "start_mb2_${letter}" ''
      mpirun --allow-run-as-root -np $1 --hostfile $2 MADbench2.x ${pixels} 8 1 8 8 1 1
    '';

  start_mb2_S = start_mb_f "S" "${builtins.toString MB2Small}";
  start_mb2_M = start_mb_f "M" "${builtins.toString MB2Med}";
  start_mb2_B = start_mb_f "B" "${builtins.toString MB2Big}";

  plop = pkgs.writeScriptBin "plop" ''
    cd ${nfsMountPoint}
    get_host_file > my_hosts
    # NBPROCS=$(cat my_hosts | cut -d = -f 2 | awk '{sum+=$1;}END {print sum;}')
    start_mb2_$1 ${builtins.toString nbProcs} my_hosts
  '';
}
