{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/22.05";
  };

  outputs = { self, nixpkgs }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
    flakeImage = pkgs.dockerTools.pullImage {
      imageName = "nixpkgs/nix-flakes";
      imageDigest =
        "sha256:653ac11d23bbe5b9693f156cafeb97c7e8000b187d1bafec3798f6c92238fde2";
      sha256 = "15543hvgw2g8aadkx335pprrxq3ldcv93a9qq9c4am0rbkw8prrw";
      finalImageName = "nixpkgs/nix-flakes";
      finalImageTag = "nixos-21.11";
    };
  in
  {

    packages.${system} = rec {
      npb = pkgs.callPackage ./npb/default.nix {};
      mb2 = pkgs.callPackage ./MADbench2/default.nix {};
      mdbook-admonish = pkgs.callPackage ./tuto/mdbook-admonish.nix { };

      doc = pkgs.stdenv.mkDerivation {
        name = "nxcDoc";
        src = ./tuto;
        nativeBuildInputs = with pkgs; [ mdbook mdbook-mermaid mdbook-admonish ]; # mdbook-linkcheck pour checker la validiter des liens
        buildCommand = ''
          mkdir $out
          cp -r --no-preserve=mode $src/* .
          mdbook build -d $out
        '';
      };
      slides = pkgs.stdenv.mkDerivation {
          name = "slides";
          src = ./slides;
          buildInputs = with pkgs; [
            texlive.combined.scheme-full
          ];
          buildPhase = ''
            mkdir -p $out
            pdflatex main.tex
            pdflatex main.tex
            pdflatex main.tex
            mv main.pdf $out/slides_tutorial.pdf
          '';
          installPhase = ''
            echo 'Skipping installPhase'
          '';
        };
      imageCIdoc = pkgs.dockerTools.buildImageWithNixDb {
        name = "guilloteauq/nxc-tuto";
        tag = "tuto";
        fromImage = flakeImage;
        contents = with pkgs; [ mdbook mdbook-mermaid mdbook-admonish ]; # mdbook-linkcheck pour checker la validiter des liens
      };
  };
};
}
